let http = require("http");
let port = 4000;
let server = http.createServer((request, response)=>{
    if(request.url === "/greeting"){
        response.writeHead(200, {"Content-Type": "text/plain"});
        response.end("Hello World");
    }else{
        response.writeHead(404, {"Content-Type": "text/plain"});
        response.end("Page not found");
    }
});

server.listen(port);

console.log(`Server now running at localhost: ${port}`);
